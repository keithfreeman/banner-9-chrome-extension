# Banner 9 Printer Chrome Extension

This is the source code for creating a Chrome Extension that works with a small application, the `Banner 9 Printer Helper`, to make printing from Banner 9 Admin Pages easy and painless.  The extension grabs the HTML markup from the Admin Page and posts it to the small sister application.  The `Printer Helper` then parses all of the form data and recreates it using standard Bootstrap 3 elements with a VueJS-based UI.  Grids can be sorted, columns can be shifted left or right or hidden entirely.  Sections can be hidden.  Best of all, because it's based on Bootstrap, it's responsive and prints really well.

There are several advantages to using the `Banner 9 Printer Helper`.  Often times, not all columns can be printed natively from Banner 9.  Some, but not always all, columns can be sorted.  For many grids, there are just too many columns to print natively with any level readability.  The `Printer Helper` lets you remove unnecessary columns so you can get exactly what you want without sacrificing usability.  Of course, no program is perfect.  There are some Admin Pages that are difficult to print in their entirety.  Still, with the scalability of the underlying Bootstrap style, you will likely achieve far better results than you can with native Banner 9 printing.

# How to Use This Source Code

Before doing anything, you should read up on extensions using the developer docs that Google provides.  You can find them [here](https://developer.chrome.com/extensions).

tl;dr

1. Install the [Banner 9 Printer Helper](https://gitlab.com/keithfreeman/banner-9-printer-helper)
2. Fork or pull this project down
3. Edit the `manifest.json` file
4. Edit the `background.js` file
5. Edit the `print.js` file
6. Publish to the Chrome App Store

Of course, you must have the `Banner 9 Printer Helper` up and running for this extension to do anything for you.  Be sure to have that up first.  Installation and configuration instructions can be found in the readme file for the project found [here](https://git.wayne.edu/ee6515/banner-9-printer-helper).

I recommend creating an extension specifically for your institution.  This is done by editing three files in this repository after pulling it down.  In the `manifest.json`, look for the line below:

```
            "matches" : ["https://*.school.edu/BannerAdmin/*"],
```

This clause tells Chrome on what pages to add the content scripts (`jquery-2.2.4.min.js` and `print.js`) by matching the URL of the page (including iframes) to the pattern defined here.  If you have multiple environments (like Production, Staging, and Development, e.g.), try to make this pattern fit all of them, so you can use it with all environments.  Also be sure to make it specific enough to work only on Admin Pages, and no where else.  The extension won't do anything, but no reason to put it places it has no business being.
 
 The second file to edit is the `background.js` file.  Look for this line:
 
 ```
                 pageUrl: {hostContains: 'school.edu', pathPrefix: '/applicationNavigator'},
 ```
 
 This lets Chrome know when to allow the extension button to be active.  On non-matching pages, the button is grayed out.  Otherwise it is colored in.  It should be general enough to match the Application Navigator URL for any environment you may have, but not match anything else.  The extension won't do anything bad, but you may have users wondering why it is active on pages it shouldn't be.
 
 The last file to edit is the `print.js` file.  Look for this line:
 
 ```
        var form = $('<form target="_blank" action="https://printer-helper.school.edu/print" method="post"><input type="hidden"
 ```

This is what posts the HTML markup of the Admin Page to the `Banner 9 Printer Helper`.  You need to replace the generic domain above with the domain where your installation of the sister application can be found.  Be sure to leave the `/print` path in there!

Publishing to the Chrome App Store is relatively simple.  You'll need a developer account, and you may have to pay a one-time $5 fee to Google.  You can make the listing public or private.  Because it's geared to a single institution, private probably makes the most sense.

# Branding and Credit

Feel free to change the icon images found in the `images` folder.  They were styled specifically for the school where I originally developed the `Banner 9 Printer Helper`.  Or leave them as-is. That's up to you.

Lastly, you can certainly edit the `manifest.json` file to reflect a different author.  I won't stop you.  That property is optional in the manifest, and probably nobody will ever see it anyway.