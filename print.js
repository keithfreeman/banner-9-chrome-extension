window.addEventListener("message", receiveMessage, false);

function receiveMessage(event)
{
    if (event.data == 'Banner-9-Printer-Helper') {
        var form = $('<form target="_blank" action="https://printer-helper.school.edu/print" method="post"><input type="hidden" name="title" value="' + encodeURIComponent($('div.print-title').text().trim()) + '" /><input type="hidden" name="incoming" value="' + encodeURIComponent($('div.workspace-container').html()) + '" /><input type="hidden" name="footer" value="' + encodeURIComponent($('footer.workspace-footer').html()) + '" /></form>').appendTo($(document.body));
        form.submit();
        form.remove();
    }
}